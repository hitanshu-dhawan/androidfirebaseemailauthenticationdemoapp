package com.hitanshudhawan.emailauthentication;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class AuthActivity extends AppCompatActivity {

    private EditText signInEmailEditText;
    private EditText signInPasswordEditText;
    private Button signInButton;

    private EditText signUpEmailEditText;
    private EditText signUpPasswordEditText;
    private EditText signUpConfirmPasswordEditText;
    private Button signUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        signInEmailEditText = (EditText) findViewById(R.id.signInEmailEditText);
        signInPasswordEditText = (EditText) findViewById(R.id.signInPasswordEditText);
        signInButton = (Button) findViewById(R.id.signInButton);

        signUpEmailEditText = (EditText) findViewById(R.id.signUpEmailEditText);
        signUpPasswordEditText = (EditText) findViewById(R.id.signUpPasswordEditText);
        signUpConfirmPasswordEditText = (EditText) findViewById(R.id.signUpConfirmPasswordEditText);
        signUpButton = (Button) findViewById(R.id.signUpButton);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = signInEmailEditText.getText().toString();
                String password = signInPasswordEditText.getText().toString();
                if (!email.isEmpty() && !password.isEmpty()) {
                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password).addOnCompleteListener(AuthActivity.this ,new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // everything is good.
                                Intent mainIntent = new Intent(AuthActivity.this, MainActivity.class);
                                startActivity(mainIntent);
                                finish();
                            }
                        }
                    });
                }
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = signUpEmailEditText.getText().toString();
                String password1 = signUpPasswordEditText.getText().toString();
                String password2 = signUpConfirmPasswordEditText.getText().toString();
                if (!email.isEmpty() && !password1.isEmpty() && !password2.isEmpty() && password1.equals(password2)) {
                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password1).addOnCompleteListener(AuthActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // everything is good.
                                Intent mainIntent = new Intent(AuthActivity.this, MainActivity.class);
                                startActivity(mainIntent);
                                finish();
                            }
                        }
                    });
                }
            }
        });
    }
}
